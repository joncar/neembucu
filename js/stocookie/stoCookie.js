/*! StoCookie - cookie law notification and custom alerts
 *  jQuery plugin
 * ================
 *
 * @Author  Nicola Franchini
 * @version 1.1
 * @Licence: http://codecanyon.net/wiki/support/legal-terms/licensing-terms/
 */
(function($){
    $.fn.extend({
        // plugin name - stoCookie
        stoCookie: function(options) {
          // default options
          var defaults = {
              message: 'Set the "message" option',  // custom message
              cookieName: 'stoCookie',            // name of the cookie to set to remember acceptance
              cookieLife: 365,                    // duration in days of the acceptance cookie
              mode: 'session',                    // 'cookie' | 'session' | 'always'
              scrollClose: false,                 // hide banner on scroll, use true or 'y' to activate
              position: 'top',                    // 'top' | 'bottom'
              background: '#ffffff',              // banner background color
              color: '#333333',                   // banner text color
              linkColor: '#1b98ff',               // link color
              fontFamily: false,                  // font family used for the popup: false | 'sans' | 'your_custom_font'
              fontSize: false,                    // font size used for the popup
              margin: '',                         // banner margin in pixel (e.g: 10px)
              maxw: '',                           // banner max-width in pixel (e.g: 1200px)
              slide: false,                       // slide effect, use true or 'y' to activate
              fade: false,                        // fade effect, use true or 'y' to activate
              btnText: false,                     // Agree button text, false to hide the button
              buttonBg: '#333333',                // button background
              buttonColor: '#ffffff',             // button text color
              buttonAlign: 'center',              // 'left' | 'center' | 'right'
              buttonCorner: '',                   // button border radius in pixel
              bannerCorner: '',                   // banner border radius in pixel
              innerClass: '',                     // optional additional class to .biscotto
              added: function() {},               // callback after the banner has been added
              removed: function() {},             // callback after the banner has been removed
              accepted: function() {}             // callback after the acceptation check
          };

          var options = $.extend(defaults, options);

          return this.each(function() {

              var obj = $(this);

              /**
              * normalize options
              */ 
              if (options.btnText && options.btnText.length > 0) {
                  options.btnText = $.parseHTML(options.btnText);
              } else {
                options.btnText = false;
              }
              if (!options.slide || options.slide.length < 1) {
                options.slide = false;
              }
              if (!options.fade || options.fade.length < 1) {
                options.fade = false;
              }
              if (!options.scrollClose || options.scrollClose.length < 1) {
                options.scrollClose = false;
              }
              if (!options.fontFamily || options.fontFamily.length < 1) {
                options.fontFamily = false;
              }

              options.message = $.parseHTML(options.message);

              /**
              * check cookie
              */ 
              function readCookie(name) {
                  var nameEQ = name + "=";
                  var ca = document.cookie.split(';');
                  for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                  }
                  return null;
              }

              /**
              * set cookie
              */ 
              function createCookie(name, value, days) {
                  if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                  }
                  else var expires = "";
                  document.cookie = name+"="+value+expires+"; path=/";
              }

              /**
              * set session
              */ 
              function setSession(key, value) {
                  sessionStorage.setItem(key, value);
              }

              /**
              * close window
              */ 
              function closeBiscotto(){
                // close banner
                $('.biscotto').removeClass('on');

                // set session
                if (options.mode == 'session') {
                  setSession(options.cookieName,'agreed');
                }
                // set cookie
                if (options.mode == 'cookie') {
                  createCookie(options.cookieName,'agreed', options.cookieLife);
                }

                setTimeout(function() {
                  $('.biscotto').remove();
                  // callback
                  options.removed.call(this);
                }, 800);
              }
              
              /**
              * call closeBiscotto() on scroll
              */ 
              function checkScroll(){

                  var startscroll = window.pageYOffset;

                  $(window).on('scroll', function() {
                      var moved = Math.abs(startscroll - window.pageYOffset);
                      var limit = $('.biscotto').height();
                      if (moved > limit) {
                        setTimeout(function() {
                          closeBiscotto();
                        }, 1500);
                      }
                  });
              }

              /**
              * Display banner
              */ 
              function showBiscotto() {
                  
                  var button = "";

                  if (options.btnText !== false) {
                    button = '<button class="close-biscotto"></button>';
                    button = '<p class="biscotto-agree">'+ button +'</p>';
                  }
                  
                  var biscotto = '<div class="biscotto ' + options.innerClass + '">'
                  + '<div class="biscotto-inner"><span class="biscotto-times"></span>'
                  + '<div class="biscotto-container">'
                  + '<div class="cook-message"></div>'
                  + button + '</div></div></div>';

                  obj.append(biscotto);

                  if (options.fontFamily !== false) {
                    $('.cook-message').css('font-family', options.fontFamily);
                  }
                  if (options.fontSize !== false) {
                    $('.cook-message').css('font-size', options.fontSize);
                  }

                  $('.cook-message').html(options.message);
                  $('.close-biscotto').html(options.btnText);

                  var bistanza = $('.biscotto');

                  bistanza.css('padding', options.margin);
                  
                  if (options.slide !== false) {
                      bistanza.addClass('sc-slide');
                  }
                  if (options.fade !== false) {
                      bistanza.addClass('sc-fade');
                  }

                  if (options.position == 'top') {
                    bistanza.addClass('sc-top').css({'padding-bottom': '15px'});
                  } else {
                    bistanza.addClass('sc-bottom').css({'padding-top': '15px'});
                  }
                  
                  var bish = $('.biscotto').outerHeight();

                  var pos = (options.position == 'top' ? 'top' : 'bottom');

                  if (options.slide !== false) {
                    bistanza.css(pos, -bish);
                  }

                  $('.biscotto a').css('color', options.linkColor);

                  $('.close-biscotto').css({ 
                    'background': options.buttonBg, 
                    'color': options.buttonColor ,
                    'border-radius': options.buttonCorner
                  });
                  
                  $('.biscotto-inner').css({ 
                    'background': options.background, 
                    'color': options.color,
                    'max-width' : options.maxw,
                    'border-radius' : options.bannerCorner
                  });
                  
                  $('.biscotto-agree').css('text-align', options.buttonAlign);


                  if (options.scrollClose !== false) {
                    checkScroll();
                  }

                  bistanza.offset();
                  bistanza.addClass('sc-animate');
                  bistanza.addClass('on');

                  // callback
                  options.added.call(this);
              }

              function init(){
                
                var visitor = false;

                if (options.mode == 'cookie') {
                  visitor = readCookie(options.cookieName);
                }

                if (options.mode == 'session') {
                  visitor = sessionStorage.getItem(options.cookieName);
                }

                if (visitor === 'agreed') {
                   options.accepted.call(this);
                } else {

                  $(document).on('click', '.biscotto-times', function(){
                    closeBiscotto();
                  });

                  $(document).on('click', '.close-biscotto', function(){
                    closeBiscotto();
                  });
                  showBiscotto();
                }
              }
              init();
          });
        }
    });
})(jQuery);