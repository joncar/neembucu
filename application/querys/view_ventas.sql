DROP VIEW IF EXISTS view_ventas;
CREATE VIEW view_ventas AS
SELECT 
ventas.id as nro_compra,
user.nombre AS Nombre,
user.apellido_paterno AS '1º Apellido',
user.apellido_2 AS '2º Apellido',
user.dni as 'DNI',
DATE_FORMAT(user.fecha_nacimiento,'%d/%m/%Y') as 'Fecha de nacimiento',
DATE_FORMAT(user.fecha_caducidad,'%d/%m/%Y') as 'Fecha de caducidad',
user.fecha_caducidad as 'Caducidad DNI',
user.email,
user.telefono,
user.referencia_grupo as 'Codigo Grupo',
user.Instituto,
ventas.productos,
FORMAT(ventas.precio,2,'de_DE') AS 'Importe de venta',
ventas.cantidad,
DATE_FORMAT(ventas.fecha_compra,'%d/%m/%Y %h:%i') as 'Fecha de compra',
ventas.procesado
FROM ventas
INNER JOIN user on user.id = ventas.user_id
ORDER BY ventas.id DESC