<ul id="menu" class="clearfix">
  <li class="<?= $this->router->fetch_class()=='main'?'current':'' ?>">
    <a href="<?= site_url() ?>">Inici</a>
  </li>
  <li class="<?= @$link=='nosaltres'?'current':'' ?>">
    <a href="<?= base_url('p/nosaltres') ?>">Nosaltres</a>
  </li>
  <li class="<?= @$link=='serveis'?'current':'' ?>">
    <a href="<?= base_url('serveis') ?>">Serveis</a>
  </li>
  <li class="<?= @$link=='blog'?'current':'' ?>">
    <a href="<?= base_url('blog') ?>">Blog</a>
  </li>
  <li class="<?= @$link=='contacte'?'current':'' ?>">
    <a href="<?= base_url('p/contacte') ?>">Contacte</a>
  </li>
  <li class="phoneonlysm">
    <a href="#" style="font-size:18px;">
      <b><i class="licon-telephone"></i> 987.654.3210</b>
    </a>
  </li>
</ul>