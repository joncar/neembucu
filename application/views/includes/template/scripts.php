
<script src="<?= base_url() ?>theme/theme/js/jquery.js"></script> 
<script src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery.fancybox.pack.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery.fancybox-media.js"></script>
<script src="<?= base_url() ?>theme/theme/js/owl.js"></script>
<script src="<?= base_url() ?>theme/theme/js/appear.js"></script>
<script src="<?= base_url() ?>theme/theme/js/wow.js"></script>
<script src="<?= base_url() ?>theme/theme/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/validate.js"></script>
<script src="<?= base_url() ?>theme/theme/js/script.js"></script>
<script src="<?= base_url() ?>theme/theme/js/color-settings.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>
<script src="<?= base_url() ?>theme/theme/js/map-script.js"></script>
<script>
    var captcha = '1';
    function contacto(f){
        if(captcha===''){
            $(".g-recaptcha").show();
            captcha = 1;
        }else{
            var post_data = new FormData(f);           
            //Ajax post data to server
            $.ajax({
                url: URL+'paginas/frontend/contacto',
                data: post_data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response){ 
                    response = JSON.parse(response);                                           
                    $("#result").hide().html(response.text).slideDown();
                }
            })
        }

        return false;
    }
</script>


<!---- Social Feed ---->
<!-- jQuery -->
<!-- Codebird.js - required for TWITTER -->
<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<!-- doT.js for rendering templates -->
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<!-- Moment.js for showing "time ago" and/or "date"-->
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<!-- Moment Locale to format the date to your language (eg. italian lang)-->
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
    $(document).ready(function(){
        $('.facebookPage').socialfeed({            
            facebook:{
                accounts: ['@mallorcaislandfestival'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 6,                                   //Integer: max number of posts to load
                access_token: 'EAAHYAimLIwABAGNSwaFuGcjZCjFRv2XuRoV4KQyxswD3LvNWGAzfHKGCRSiZBo0PZAckFebZAZApYL2snIlZCZChHka63uxsJagjN7100jTTqZBBmqRu5uTn70236Pis1cCvZBrqUFoDCME543Pn2ctybz0UZAK09o9FZAy3ALsP62xRgZDZD'  //String: "APP_ID|APP_SECRET"
            },
            length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
            show_media:true,                                //Boolean: if false, doesn't display any post images
            media_min_width: 300,                           //Integer: Only get posts with images larger than this value
            update_period: 5000,                            //Integer: Number of seconds before social-feed will attempt to load new posts.
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)            
            callback:function(){
                $('.lightbox-image').fancybox({
                    openEffect  : 'fade',
                    closeEffect : 'fade',
                    helpers : {
                        media : {}
                    }
                });
            }
        });
    });
</script>