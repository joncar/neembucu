<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($description) ?'': $description ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = '<?= base_url() ?>';</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <?php if(!empty($image)): ?>
    	<!-- for Facebook -->
        <meta property="og:title" content="<?= $title ?>"/>
        <meta property="og:site_name" content="<?= $this->db->get('ajustes')->row()->titulo ?>"/>
        <meta property="og:image" content="<?= $image ?>" />
        <meta property="og:url" content="<?= $url ?>" />
        <meta property="og:description" content="<?= $description ?>">
        
        <!-- for Twitter -->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="<?= $title ?>" />
        <meta name="twitter:description" content="<?= $description ?>" />
        <meta name="twitter:image" content="<?= $image ?>" />
    <?php endif ?>
	<!-- Stylesheets -->
	<link href="<?= base_url() ?>theme/theme/css/bootstrap.css" rel="stylesheet">
	<!-- Social-feed css -->
	<link href="<?= base_url() ?>css/social.css" rel="stylesheet" type="text/css">
	<!--- Custom ---->
	<link href="<?= base_url() ?>theme/theme/css/style.css?v=1.3" rel="stylesheet">
	<link href="<?= base_url() ?>theme/theme/css/responsive.css" rel="stylesheet">

	<!--Color Themes-->
	<link id="theme-color-file" href="<?= base_url() ?>theme/theme/css/color-themes/default-theme.css" rel="stylesheet">
</head>

<body>
	<div class="page-wrapper">
 	
	    <!-- Preloader -->
	    <div class="preloader"></div>	
		<?php 
			if(empty($editor)){
				$this->load->view($view); 
			}else{
				echo $view;
			}
		?>		
		
	</div>
	<!--End pagewrapper-->

	<!--Scroll to top-->
	<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>
	<?php $this->load->view('includes/template/scripts'); ?>
	<!--End Scroll to top-->
</body>
</html>