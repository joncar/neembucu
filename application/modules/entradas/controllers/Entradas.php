<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Entradas extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function informes(){
        	$this->as['informes'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_800','image',array('path'=>'img/blog','width'=>'800px','height'=>'632px'))
                 ->field_type('foto_792','image',array('path'=>'img/blog','width'=>'797px','height'=>'312px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')                 
                 ->display_as('foto','Foto Cuadro inferior');
            $crud->field_type('idioma','hidden','es');
            $crud->field_type('user','string',$this->user->nombre)
                 ->set_field_upload('foto_grande','img/blog');


            $crud->where('blog.blog_categorias_id',5)
            	 ->display_as('blog_subcategorias_id','Categoria')
            	 ->field_type('blog_categorias_id','hidden',5)
            	 ->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>5))            	 
            	 ->field_type('tags','tags')
            	 ->field_type('foto','image',array('path'=>'img/blog','width'=>'397px','height'=>'315px'))
            	 ->field_type('vistas','hidden',0)
            	 ->columns('blog_subcategorias_id','foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function ley_5189_2014(){
        	$this->as['ley_5189_2014'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_800','image',array('path'=>'img/blog','width'=>'800px','height'=>'632px'))
                 ->field_type('foto_792','image',array('path'=>'img/blog','width'=>'797px','height'=>'312px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')
                 ->display_as('foto','Foto Cuadro inferior');
            $crud->field_type('idioma','hidden','es');
            $crud->field_type('user','string',$this->user->nombre)
                 ->set_field_upload('foto_grande','img/blog');


            $crud->where('blog.blog_categorias_id',3)
            	 ->display_as('blog_subcategorias_id','Categoria')
            	 ->field_type('blog_categorias_id','hidden',3)
            	 ->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>3))            	 
            	 ->field_type('tags','tags')
            	 ->field_type('foto','image',array('path'=>'img/blog','width'=>'397px','height'=>'315px'))
            	 ->field_type('vistas','hidden',0)
            	 ->columns('blog_subcategorias_id','foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function ley_5181_2016(){
        	$this->as['ley_5181_2016'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_800','image',array('path'=>'img/blog','width'=>'800px','height'=>'632px'))
                 ->field_type('foto_792','image',array('path'=>'img/blog','width'=>'797px','height'=>'312px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')
                 ->display_as('foto','Foto Cuadro inferior');
            $crud->field_type('idioma','hidden','es');
            $crud->field_type('user','string',$this->user->nombre)
                 ->set_field_upload('foto_grande','img/blog');


            $crud->where('blog.blog_categorias_id',4)
            	 ->display_as('blog_subcategorias_id','Categoria')
            	 ->field_type('blog_categorias_id','hidden',4)
            	 ->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>4))            	 
            	 ->field_type('tags','tags')
            	 ->field_type('foto','image',array('path'=>'img/blog','width'=>'397px','height'=>'315px'))
            	 ->field_type('vistas','hidden',0)
            	 ->columns('blog_subcategorias_id','foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function documentos(){
        	$this->as['documentos'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_800','image',array('path'=>'img/blog','width'=>'800px','height'=>'632px'))
                 ->field_type('foto_792','image',array('path'=>'img/blog','width'=>'797px','height'=>'312px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')
                 ->display_as('foto','Foto Cuadro inferior');
            $crud->field_type('idioma','hidden','es');
            $crud->field_type('user','string',$this->user->nombre)
                 ->set_field_upload('foto_grande','img/blog');


            $crud->where('blog.blog_categorias_id',2)
            	 ->display_as('blog_subcategorias_id','Categoria')
            	 ->field_type('blog_categorias_id','hidden',2)
            	 ->set_relation('blog_subcategorias_id','blog_subcategorias','blog_subcategorias_nombre',array('blog_categorias_id'=>2))            	 
            	 ->field_type('tags','tags')
            	 ->field_type('foto','image',array('path'=>'img/blog','width'=>'397px','height'=>'315px'))
            	 ->field_type('vistas','hidden',0)
            	 ->columns('blog_subcategorias_id','foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function secretarias(){
        	$this->as['secretarias'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_grande','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')
                 ->display_as('foto','Foto Cuadro inferior')
                 ->display_as('foto_grande','Banner');
            $crud->field_type('idioma','hidden','es')
                 ->field_type('foto_800','hidden')
                 ->field_type('foto_792','hidden');
            $crud->field_type('user','string',$this->user->nombre);


            $crud->where('blog_subcategorias_id',13)
            	 ->field_type('blog_categorias_id','hidden',1)
            	 ->field_type('blog_subcategorias_id','hidden',13)            	 
            	 ->field_type('tags','tags')
            	 ->field_type('foto','image',array('path'=>'img/blog','width'=>'921px','height'=>'639px'))
            	 ->field_type('vistas','hidden',0)
            	 ->columns('foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function eventual(){
            $this->as['eventual'] = 'blog';
            $crud = $this->crud_function('','');
            $crud->field_type('foto_grande','image',array('path'=>'img/blog','width'=>'1700px','height'=>'500px'))
                 ->field_type('foto_800','image',array('path'=>'img/blog','width'=>'800px','height'=>'632px'))
                 ->field_type('foto_792','image',array('path'=>'img/blog','width'=>'797px','height'=>'312px'))
                 ->display_as('foto_800','Foto Ampliada Home')
                 ->display_as('foto_792','Foto Horizontal Home')
                 ->display_as('foto','Foto Cuadro inferior')
                 ->display_as('foto_grande','Banner');
            $crud->field_type('idioma','hidden','es');
            $crud->field_type('user','string',$this->user->nombre);


            $crud->where('blog_categorias_id',6)
                 ->field_type('blog_categorias_id','hidden',6)
                 ->field_type('blog_subcategorias_id','hidden',130)               
                 ->field_type('tags','tags')
                 ->field_type('foto','image',array('path'=>'img/blog','width'=>'397px','height'=>'315px'))
                 ->field_type('vistas','hidden',0)
                 ->columns('foto','titulo','fecha','status','user','vistas');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function galeria(){        	
            $crud = $this->crud_function('','');
            


            $crud->field_type('tipo','dropdown',array('1'=>'Fotos','2'=>'Videos'))
                 ->field_type('enlace','image',array('path'=>'img/galeria','width'=>'800px','height'=>'600px'));
            	 
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
