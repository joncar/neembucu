<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array(),TRUE),
                    'link'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }

        public function secretarias($id){
            $id = explode('-',$id);
            $id = $id[0];

        	$blog = new Bdsource();
            //$blog->limit = array('6','0');
            $blog->order_by = array('fecha','DESC');
            $categoria = $this->db->get_where('blog_subcategorias',array('id'=>$id));
            if($categoria->num_rows()>0){
            	$categoria = $categoria->row();
            	$categoria->categoria_nombre = $this->db->get_where('blog_categorias',array('id'=>$categoria->blog_categorias_id))->row()->nombre;
	            if(!empty($_GET['direccion'])){
	                $blog->like('titulo',$_GET['direccion']);
	            }
	            if(!empty($_GET['blog_categorias_id'])){
	                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
	            }
	            //$blog->where('idioma',$_SESSION['lang']);
	            if(!empty($_GET['page'])){
	                //$blog->limit = array(6,($_GET['page']-1));
	            }else{
	                $_GET['page'] = 1;
	            }
	            $blog->where('idioma',$_SESSION['lang']);
	            $blog->where('blog_subcategorias_id',$id);
	            $blog->init('blog');
	            foreach($this->blog->result() as $n=>$b){
	                $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	                $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
	                $this->blog->row($n)->mes = strftime('%b',strtotime($b->fecha));
	                $this->blog->row($n)->dia = strftime('%d',strtotime($b->fecha));
	                $this->blog->row($n)->fecha = strftime('%d %b %Y',strtotime($b->fecha));
	                $this->blog->row($n)->texto = cortar_palabras(strip_tags($b->texto),30);
	            }
	            $this->db->where('idioma',$_SESSION['lang']);
	            $totalpages = round($this->db->get_where('blog')->num_rows()/6);
	            $totalpages = $totalpages==0?'1':$totalpages;        
	            foreach($this->blog->result() as $n=>$b){
	                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
	            }
	            if($this->blog->num_rows()>0){
	                $this->blog->tags = $this->blog->row()->tags;
	            }
	            
	            $recientes = new Bdsource();
	            $recientes->limit = array(4);                         
	            $recientes->order_by = array('id','desc');
	            $recientes->where('idioma',$_SESSION['lang']);
	            $recientes->init('blog',FALSE,'recientes');
	            foreach($this->recientes->result() as $n=>$b){
	                $this->recientes->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	                $this->recientes->row($n)->foto = base_url('img/blog/'.$b->foto);
	                $this->recientes->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
	                $this->recientes->row($n)->texto = substr($b->texto,0,80);
	            }
	            $tags = '';
	            if($this->blog->num_rows()>0){
	                foreach(explode(',',$this->blog->row(0)->tags) as $t){
	                    $tags = '<a href="#">'.$t.'</a>';
	                }
	            }

	            $page = $this->load->view($this->theme.'/documentos',array('blog'=>$this->blog),TRUE,'paginas');
				$page = $this->querys->fillFields($page,array('blog'=>$this->blog));
				$page = str_replace('[paginacion]',$this->load->view('frontend/_entry',array(),TRUE,'blog'),$page);
				$page = str_replace('[tags]',$tags,$page);
				$page = str_replace('[seccion]',$categoria->categoria_nombre,$page);
				$page = str_replace('[titulo]',$categoria->blog_subcategorias_nombre,$page);

				$categoriaa = $this->db->get_where('blog_categorias',array('id'=>$categoria->blog_categorias_id))->row();
                $banner = !empty($this->blog->banner)?$this->blog->banner:$categoriaa->banner;                

				$page = str_replace('[banner]',base_url('img/blog/'.$banner),$page);

	            $this->loadView(
	                array(
	                    'view'=>'read',	                    
	                    'page'=>$page,
	                    'link'=>$categoria->blog_subcategorias_nombre,
	                    'title'=>ucfirst(str_replace('-',' ',$categoria->blog_subcategorias_nombre))
	                )
	            );        	
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        public function documentos($id){
        	$id = explode('-',$id);
            $id = $id[0];

        	$blog = new Bdsource();
            //$blog->limit = array('6','0');
            $blog->order_by = array('fecha','DESC');
            $categoria = $this->db->get_where('blog_subcategorias',array('id'=>$id));
            if($categoria->num_rows()>0){
            	$categoria = $categoria->row();
            	$categoria->categoria_nombre = $this->db->get_where('blog_categorias',array('id'=>$categoria->blog_categorias_id))->row()->nombre;
	            if(!empty($_GET['direccion'])){
	                $blog->like('titulo',$_GET['direccion']);
	            }
	            if(!empty($_GET['blog_categorias_id'])){
	                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
	            }
	            //$blog->where('idioma',$_SESSION['lang']);
	            if(!empty($_GET['page'])){
	                //$blog->limit = array(6,($_GET['page']-1));
	            }else{
	                $_GET['page'] = 1;
	            }
	            $blog->where('idioma',$_SESSION['lang']);
	            $blog->where('blog_subcategorias_id',$id);
	            $blog->init('blog');
	            foreach($this->blog->result() as $n=>$b){
	                $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	                $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
	                $this->blog->row($n)->mes = strftime('%b',strtotime($b->fecha));
	                $this->blog->row($n)->dia = strftime('%d',strtotime($b->fecha));
	                $this->blog->row($n)->fecha = strftime('%d %b %Y',strtotime($b->fecha));
	                $this->blog->row($n)->texto = cortar_palabras(strip_tags($b->texto),30);
	            }
	            $this->db->where('idioma',$_SESSION['lang']);
	            $totalpages = round($this->db->get_where('blog')->num_rows()/6);
	            $totalpages = $totalpages==0?'1':$totalpages;        
	            foreach($this->blog->result() as $n=>$b){
	                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
	            }
	            if($this->blog->num_rows()>0){
	                $this->blog->tags = $this->blog->row()->tags;
	            }
	            
	            $recientes = new Bdsource();
	            $recientes->limit = array(4);                         
	            $recientes->order_by = array('id','desc');
	            $recientes->where('idioma',$_SESSION['lang']);
	            $recientes->init('blog',FALSE,'recientes');
	            foreach($this->recientes->result() as $n=>$b){
	                $this->recientes->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
	                $this->recientes->row($n)->foto = base_url('img/blog/'.$b->foto);
	                $this->recientes->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
	                $this->recientes->row($n)->texto = substr($b->texto,0,80);
	            }
	            $tags = '';
	            if($this->blog->num_rows()>0){
	                foreach(explode(',',$this->blog->row(0)->tags) as $t){
	                    $tags = '<a href="#">'.$t.'</a>';
	                }
	            }

	            $page = $this->load->view($this->theme.'/documentos',array('blog'=>$this->blog),TRUE,'paginas');
				$page = $this->querys->fillFields($page,array('blog'=>$this->blog));
				$page = str_replace('[paginacion]',$this->load->view('frontend/_entry',array(),TRUE,'blog'),$page);
				$page = str_replace('[tags]',$tags,$page);
				$page = str_replace('[seccion]',$categoria->categoria_nombre,$page);
				$page = str_replace('[titulo]',$categoria->blog_subcategorias_nombre,$page);

				$categoriaa = $this->db->get_where('blog_categorias',array('id'=>$categoria->blog_categorias_id))->row();
                $banner = !empty($this->blog->banner)?$this->blog->banner:$categoriaa->banner;                

				$page = str_replace('[banner]',base_url('img/blog/'.$banner),$page);

	            $this->loadView(
	                array(
	                    'view'=>'read',	                    
	                    'page'=>$page,
	                    'link'=>$categoria->blog_subcategorias_nombre,
	                    'title'=>ucfirst(str_replace('-',' ',$categoria->blog_subcategorias_nombre))
	                )
	            );
        	}
        }

        
    }
?>
