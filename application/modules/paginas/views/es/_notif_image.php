<?php if(empty($type)): ?>
<div class="image">
    <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->{!empty($image)?$image:'foto'}) ?>" alt="<?= $notif->titulo ?>" />
    <div class="overlay-box">
        <div class="content">
            <div class="tag">
                <a href="<?= base_url('blog/'.toUrl($notif->id.'-'.$notif->titulo)) ?>">
                    <?= @$notif->blog_subcategorias->blog_subcategorias_nombre ?>
                </a>
            </div>
            <h3>
                <a href="<?= base_url('blog/'.toUrl($notif->id.'-'.$notif->titulo)) ?>">
                    <?= $notif->titulo ?>
                </a>
            </h3>
            <ul class="post-meta">
                <li><span class="icon qb-clock"></span><?= $notif->fecha; ?></li>
                <li><span class="icon qb-eye"></span><?= $notif->vistas ?></li>
            </ul>
        </div>
    </div>
</div>
<?php elseif($type=='article'): ?>
	<article class="widget-post">
        <figure class="post-thumb">
        	<a href="<?= $notif->link ?>">
        		<img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="">
        	</a>
        	<div class="overlay">
        		<span class="icon qb-play-arrow"></span>
        	</div>
    	</figure>
        <div class="text">
        	<a href="<?= $notif->link ?>"><?= $notif->titulo ?></a>
        </div>
        <div class="post-info"><?= $notif->fecha ?></div>
    </article>
<?php endif ?>