<?php $blog = $this->db->get_where('blog',array('id'=>'340'))->row(); ?>
[menu]
<div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align" style="margin-top:10px; background:url([base_url]theme/theme/images/background-contacto.jpg)">
    <div class="realfactory-header-transparent-substitute"></div>
    <div class="realfactory-page-title-overlay"></div>
    <div class="realfactory-page-title-container realfactory-container">
        <div class="realfactory-page-title-content realfactory-item-pdlr">
            <h1 class="realfactory-page-title"><?= $blog->titulo ?></h1>           
            <ul class="page-title-breadcrumb breadcump">
            	<li><a href="#"><span class="fa fa-home"></span>Inicio</a></li>                
            </ul>	
            
        </div>
    </div>
</div>
    
	<!--Contact Section-->
    <section class="contact-section">
    	<div class="auto-container">            
            <div class="row clearfix">
            	<!--Form Column-->
            	<div class="form-column col-md-12 col-sm-12 col-xs-12">
                	<?= $blog->texto ?>
                </div>
                
            </div>
            
        </div>
    </section>
    <!--End Contact Section-->
    [footer]