<!--Main Footer-->
    <footer class="main-footer">
    	<div class="widgets-section">
        	<div class="auto-container">
            	<div class="row clearfix">
                	
                    <!--Widget Column / Reviews Widget-->
                    <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    	<div class="footer-widget reviews-widget">
                        	<h2>Lo más visto</h2>

                            <?php
                                $this->db->order_by('vistas','DESC');
                                $this->db->limit(3);
                                foreach($this->querys->get_blog()->result() as $notif):                                
                            ?>
                                <!--Review Block-->
                                <div class="news-info">
                                    <div class="inner-box">
                                        <div class="image">
                                            <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="<?= $notif->titulo ?>" />
                                            <a class="overlay" href="<?= $notif->link ?>"><span class="icon fa fa-play"></span></a>
                                        </div>
                                        <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                        <div class="post-date"><?= $notif->fecha ?></div>
                                    </div>
                                </div>
                            <?php endforeach ?>                            
                            
                        </div>
                    </div>
                    
                    <!--Widget Column / Instagram Widget-->
                    <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    	<div class="footer-widget isntagram-widget">
                        	<h2>ULTIMAS Fotos</h2>
                            <div class="clearfix">
                                <?php 
                                    $this->db->limit(6);
                                    $this->db->order_by('id','DESC');
                                    foreach($this->db->get_where('galeria',array('tipo'=>1))->result() as $f): ?>
                            	   <figure class="image"><a href="<?= base_url('img/galeria/'.$f->enlace) ?>" class="lightbox-image"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/galeria/'.$f->enlace) ?>" alt=""></a></figure>
                                <?php endforeach ?>
                            </div>
                        </div>
                        
                        <div class="footer-widget popular-tags">
                        	<h2>Tags más usados</h2>
                            <?php foreach(explode(',',$this->db->get_where('blog',array('tags !='=>''))->row()->tags) as $t): ?>
                                <a href="<?= base_url('blog/') ?>?direccion=<?= $t ?>"><?= $t ?></a>
                            <?php endforeach ?>
                        </div>
                    </div>
                    
                    <!--Widget Column / Popular Widget-->
                    <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    	<div class="footer-widget popular-widget">
                        	<h2>Noticias recientes</h2>
                            <!--News Info-->
                            <?php
                                $this->db->order_by('id','DESC');
                                $this->db->limit(3);
                                foreach($this->querys->get_blog()->result() as $notif):                                
                            ?>
                                <!--Review Block-->
                                <div class="news-info">
                                    <div class="inner-box">
                                        <div class="image">
                                            <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="<?= $notif->titulo ?>" />
                                            <a class="overlay" href="<?= $notif->link ?>"><span class="icon fa fa-play"></span></a>
                                        </div>
                                        <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                        <div class="post-date"><?= $notif->fecha ?></div>
                                    </div>
                                </div>
                            <?php endforeach ?>                            
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--Footer Bottom-->
        <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="row clearfix">
                	<!--Column-->
                    <div class="column col-md-3 col-sm-12 col-xs-12">
                    	<div class="logo">
                        	<a href="<?= base_url() ?>"></a>
                        </div>
                    </div>
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	<div class="text">
                    	<li><strong>DIRECCIÓN:</strong> 14 de Mayo y Avenida Mariscal López, Alberdi, Paraguay </li>
                    	<strong>WEB:</strong> <a href="http://www.neembucu.gov.py"> www.neembucu.gov.py</a><br>
                    	<strong>EMAIL:</strong>  <a href="mailto:adm@neembucu.gov.py">   adm@neembucu.gov.py</a><br>
                    	<strong>TELÉFONO ATENCIÓN CIUDADANA:</strong>  <a href="tel:+595786232210">  +595 786 232 210</a>
                    	
                    	
                    
                    	</div>
                    </div>
                    <!--Column-->
                    <div class="column col-md-3 col-sm-12 col-xs-12">
                    	<ul class="social-icon-one">
                        	<li><a href="https://www.facebook.com/GobernaciondelXIIDepartamentodeNeembucu/" target="_blank"><span class="fa fa-facebook"></span></a></li>
                            <li class="twitter"><a href="https://twitter.com/gobernacionde12" target="_blank"><span class="fa fa-twitter"></span></a></li>
                            <li class="instagram"><a href="<?= base_url('contacto.html') ?>"><span class="fa fa-envelope"></span></a></li>
                            <li class="rss"><a href="tel:+595786232210"><span class="fa fa-phone"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Copyright Section-->
            <div class="copyright-section">
                <div class="auto-container">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <ul class="footer-nav">
                                <li><a href="<?= base_url() ?>">Inicio</a></li>
                                <li><a href="#">Institucional</a></li>                                
                                <li><a href="<?= base_url('contacto') ?>.html">Contacto</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="copyright">&copy; Copyright Gobernación de Ñeembucú.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--End Main Footer-->
    
 