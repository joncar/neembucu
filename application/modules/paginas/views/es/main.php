	[menu]
    <!--Main Slider Two-->
    <section class="main-slider-two">
    	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="single-item-carousel owl-carousel owl-theme">
                <?php
                    $this->db->limit(8);
                    $this->db->order_by('fecha','DESC');
                    $ultimas_noticias = $this->querys->get_blog(array('blog_categorias_id !='=>1,'foto_800 !='=>'','foto_792 !='=>''));
                    $n = 0;
                    for($i=1;$i<=2;$i++):
                ?>
                    <!--Slide-->
                    <div class="slide">
                        <div class="clearfix">
                            <!--Slide Column-->
                            
                            <!--- Foto ampliada ----->
                            <div class="slide-column col-md-6 col-sm-12 col-xs-12">
                                <!--News Block Three-->
                                <div class="news-block-three style-two">
                                    <div class="inner-box">
                                        

                                        <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                            <?php $this->load->view('_notif_image',array('notif'=>$notif,'image'=>'foto_800')); ?>
                                        <?php endif ?>



                                    </div>
                                </div>
                            </div>
                            <!--Slide Column-->
                            <div class="slide-column col-md-6 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <!--- Foto vertical --->
                                    <div class="inner-slide col-md-12 col-sm-12 col-xs-12">
                                        <!--News Block Three-->
                                        <div class="news-block-three style-three">
                                            <div class="inner-box">



                                                <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                    <?php $this->load->view('_notif_image',array('notif'=>$notif,'image'=>'foto_792')); ?>
                                                <?php endif ?>



                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!--- Foto normal --->
                                    <div class="inner-slide col-md-6 col-sm-6 col-xs-12">
                                        <!--News Block Three-->
                                        <div class="news-block-three style-four">
                                            <div class="inner-box">
                                                <div class="image">



                                                    <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                        <?php $this->load->view('_notif_image',array('notif'=>$notif,'image'=>'foto')); ?>
                                                    <?php endif ?>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!--- Foto normal --->
                                    <div class="inner-slide col-md-6 col-sm-6 col-xs-12">
                                        <!--News Block Three-->
                                        <div class="news-block-three style-four">
                                            <div class="inner-box">
                                                <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                    <?php $this->load->view('_notif_image',array('notif'=>$notif,'image'=>'foto')); ?>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>


                
            </div>
        </div>
    </section>
    <!--End Main Slider Two-->
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side-->
                <div class="content-side pull-right col-lg-8 col-md-8 col-sm-12 col-xs-12">
					
                    <!--Economics Blog Boxed-->
                    <div class="economics-category">
                    	<!--Sec Title-->
                        <div class="sec-title">
                        	<h2>Obras</h2>
                        </div>
                        <div class="economics-items-carousel owl-carousel owl-theme">
                        
                            

                            <?php
                                $this->db->limit(24);
                                $this->db->order_by('fecha','DESC');
                                $ultimas_noticias = $this->querys->get_blog(array('blog_subcategorias_id'=>9));                                
                                if($ultimas_noticias->num_rows()==0){
                                 echo 'No existen noticias ahora misma, intenté ingresar más tarde';
                                }
                                for($i=0;$i<4;$i++):

                                foreach($ultimas_noticias->result() as $n=>$v){
                                    $ultimas_noticias->row($n)->blog_categorias = @$this->db->get_where('blog_categorias',array('id'=>$v->blog_categorias_id))->row();
                                    $ultimas_noticias->row($n)->blog_subcategorias = @$this->db->get_where('blog_subcategorias',array('id'=>$v->blog_subcategorias_id))->row();
                                    $ultimas_noticias->row($n)->link = base_url('blog/'.toUrl($v->id.'-'.$v->titulo));
                                    $ultimas_noticias->row($n)->fecha = strftime('%B, %d,%Y',strtotime($v->fecha));
                                    $ultimas_noticias->row($n)->vistas = !empty($notif->vistas)?$notif->vistas:'0';
                                    $ultimas_noticias->row($n)->titulo = cortar_palabras($v->titulo,10);
                                }
                                $n = 0;

                            ?>
                                <div class="slide">
                                	<div class="row clearfix">
                                        <div class="column col-md-6 col-sm-6 col-xs-12">
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <!--News Block Two-->
                                                <div class="news-block-two with-margin">
                                                    <div class="inner-box">
                                                        <div class="image">
                                                            <a href="<?= $notif->link ?>">
                                                                <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto_800 ?>" alt="" />
                                                            </a>
                                                            <div class="category">
                                                                <a href="<?= $notif->link ?>">
                                                                    <?= $notif->blog_categorias->nombre ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="lower-box">
                                                            <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                                            <ul class="post-meta">
                                                                <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>                                                            
                                                                <li><span class="icon fa fa-eye"></span><?= $notif->vistas ?></li>
                                                            </ul>
                                                            <div class="text"><?= $notif->texto ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            <!--Widget Post-->
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="<?= $notif->link ?>"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto ?>" alt="<?= $notif->titulo ?>"></a></figure>
                                                    <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                                    <div class="post-info"><?= $notif->fecha ?></div>
                                                </article>
                                            <?php endif ?>
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <!--Widget Post-->
                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="<?= $notif->link ?>"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto ?>" alt="<?= $notif->titulo ?>"></a></figure>
                                                    <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                                    <div class="post-info"><?= $notif->fecha ?></div>
                                                </article>
                                            <?php endif ?>
                                         </div>
                                
                                        <div class="column col-md-6 col-sm-6 col-xs-12">
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <!--News Block Two-->
                                                <div class="news-block-two with-margin">
                                                    <div class="inner-box">
                                                        <div class="image">
                                                            <a href="<?= $notif->link ?>">
                                                                <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto_800 ?>" alt="" />
                                                            </a>
                                                            <div class="category">
                                                                <a href="<?= $notif->link ?>">
                                                                    <?= $notif->blog_categorias->nombre ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="lower-box">
                                                            <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                                            <ul class="post-meta">
                                                                <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>                                                            
                                                                <li><span class="icon fa fa-eye"></span><?= $notif->vistas ?></li>
                                                            </ul>
                                                            <div class="text"><?= $notif->texto ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            <!--Widget Post-->
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="<?= $notif->link ?>"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto ?>" alt="<?= $notif->titulo ?>"></a></figure>
                                                    <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                                    <div class="post-info"><?= $notif->fecha ?></div>
                                                </article>
                                            <?php endif ?>
                                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>
                                                <!--Widget Post-->
                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="<?= $notif->link ?>"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url().'img/blog/'.$notif->foto ?>" alt="<?= $notif->titulo ?>"></a></figure>
                                                    <div class="text"><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></div>
                                                    <div class="post-info"><?= $notif->fecha ?></div>
                                                </article>
                                            <?php endif ?>
                                         </div>
                                    </div>
                                </div>
                            <?php endfor; ?>


                            
                        </div>
                    </div>
                    <!--End Category-->

                    <?php foreach($this->db->get_where('banner',array('id'=>1))->result() as $b): ?>
                        <div class="image" style="margin-bottom: 30px;">
    			            <a href="<?= $b->enlace ?>"><img class="wow fadeIn animated" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/banner/'.$b->foto) ?>" alt="" style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
    			        </div>
                    <?php endforeach ?>
                            




			        <!--Sec Title-->
                    <div class="sec-title">
                    	<h2>Llamados y Concursos</h2>
                    </div>
                    <div class="content-blocks">
                        
                        
                        <?php
                            $this->db->limit(6);
                            $this->db->order_by('fecha','DESC');
                            $ultimas_noticias = $this->querys->get_blog(array('blog_subcategorias_id'=>21));
                            if($ultimas_noticias->num_rows()==0){
                             echo 'No existen noticias ahora misma, intenté ingresar más tarde';
                            }                          
                            foreach($ultimas_noticias->result() as $n=>$v):    
                        ?>
                        <!--News Block Four-->
                        <div class="news-block-four">
                            <div class="inner-box">
                                <div class="row clearfix">
                                    <div class="image-column col-md-6 col-sm-6 col-xs-12">
                                        <div class="image">
                                            <a href="<?= $notif->link ?>">
                                                <img class="wow fadeIn animated" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto_800) ?>" alt="" style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content-box col-md-6 col-sm-6 col-xs-12">
                                        <div class="content-inner">
                                            <div class="category">
                                                <a href="<?= $notif->link ?>"><?= $notif->blog_categorias->nombre ?></a>
                                            </div>
                                            <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                            <ul class="post-meta">
                                                <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>                                                
                                                <li><span class="icon fa fa-eye"></span><?= $notif->vistas ?></li>
                                            </ul>
                                            <div class="text"><?= $notif->texto ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
            
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side pull-left col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar left-sidebar">
						
                        <!--Social Widget-->
                        <div class="sidebar-widget sidebar-social-widget">
                            <div class="sidebar-title">
                                <h2>Siguenos</h2>
                            </div>
                            <ul class="social-icon-one alternate">
                                <li><a href="https://www.facebook.com/GobernaciondelXIIDepartamentodeNeembucu/" target="_blank"><span class="fa fa-facebook"></span></a></li>
	                            <li class="twitter"><a href="https://twitter.com/gobernacionde12" target="_blank"><span class="fa fa-twitter"></span></a></li>
	                            <li class="instagram"><a href="<?= base_url('contacto.html') ?>"><span class="fa fa-envelope"></span></a></li>
	                            <li class="rss"><a href="tel:+5950000000"><span class="fa fa-phone"></span></a></li>                                
                            </ul>
						</div>
                    	<!--End Social Widget-->
                        
                        <!-- Search -->
                        <div class="sidebar-widget search-box">
                        	<form method="get" action="<?= base_url('blog') ?>">
                                <div class="form-group">
                                    <input type="search" name="direccion" value="" placeholder="Buscar" required>
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
						</div>

                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Enlaces de interés</h2>
                            </div>
                            <ul class="cat-list">
                                <?php foreach($this->db->get_where('blog',array('enlace_interes'=>'1'))->result() as $notif): ?>
                                    <li class="clearfix">
                                        <?php                                            
                                            $notif->link = empty($notif->custom_url)?base_url().'blog/'.toUrl($notif->id.'-'.$notif->titulo):base_url($notif->custom_url);
                                            $notif->fecha = date("d-m-Y",strtotime($notif->fecha));
                                            $this->load->view('_notif_image',array('type'=>'article','notif'=>$notif)); ?>
                                        
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <!--End Category Widget-->
                        
                        <!--Category Widget-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Categorias</h2>
                            </div>
                            <ul class="cat-list">
                                <?php $this->db->limit(5); ?>
                            	<?php foreach($this->db->get_where('blog_subcategorias',array())->result() as $c): $cat = @$this->db->get_where('blog_categorias',array('id'=>$c->blog_categorias_id))->row()->nombre; ?>
                                <li class="clearfix"><a href="<?= base_url(strtolower($cat).'/'.toUrl($c->id.'-'.$c->blog_subcategorias_nombre)) ?>"><?= $c->blog_subcategorias_nombre ?> <span>(<?= $this->db->get_where('blog',array('blog_subcategorias_id'=>$c->id))->num_rows(); ?>)</span></a></li>
                                <?php endforeach ?>
                                
                            </ul>
                        </div>
                        <!--End Category Widget-->

                        
                        
                        <!--NewsLetter Widget-->
                        <div class="newsletter-widget">
                            <div class="inner-box">
                                <h2>Subscribete</h2>
                                <div id="result"></div>
                                <div class="emailed-form">
                                    <form method="post" action="paginas/frontend/subscribir" onsubmit="return sendAjax(this,'#result')">
                                        <div class="form-group">
                                            <input type="email" name="email" value="" placeholder="Ingresar E-mail" required>
                                            <button type="submit" class="theme-btn">Subscribir!</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <!--News Post Widget-->
                        <div class="sidebar-widget posts-widget">
                        	
                            <!--Product widget Tabs-->
                            <div class="product-widget-tabs">
                                <!--Product Tabs-->
                                <div class="prod-tabs tabs-box">
                                
                                    <!--Tab Btns-->
                                    <ul class="tab-btns tab-buttons clearfix">
                                        <li data-tab="#prod-popular" class="tab-btn active-btn">Populares</li>
                                        <li data-tab="#prod-recent" class="tab-btn">Recientes</li>
                                        <li data-tab="#prod-comment" class="tab-btn">Comunicados</li>
                                    </ul>
                                    
                                    <!--Tabs Container-->
                                    <div class="tabs-content">
                                        
                                        <!--Tab / Active Tab-->
                                        <div class="tab active-tab" id="prod-popular">
                                            <div class="content">
                                                
                                                <?php
                                                    $this->db->order_by('vistas','DESC');
                                                    $this->db->limit(5);
                                                    foreach($this->querys->get_blog()->result() as $notif){
                                                    $this->load->view('_notif_image',array('type'=>'article','notif'=>$notif));}
                                                ?>
                                                
                                            </div>
                                        </div>
                                        
                                        <!--Tab-->
                                        <div class="tab" id="prod-recent">
                    						<div class="content">
                                                
                                                <?php
                                                    $this->db->order_by('id','DESC');
                                                    $this->db->limit(5);
                                                    foreach($this->querys->get_blog()->result() as $notif){
                                                    $this->load->view('_notif_image',array('type'=>'article','notif'=>$notif));}
                                                ?>
                                                
                                            </div>
                                        </div>
                                        
                                        <!--Tab-->
                                        <div class="tab" id="prod-comment">
                                            <div class="content">
                                                
                                                <?php
                                                    $this->db->order_by('vistas','DESC');
                                                    $this->db->limit(5);
                                                    foreach($this->querys->get_blog(array('blog_subcategorias_id'=>17))->result() as $notif){
                                                    $this->load->view('_notif_image',array('type'=>'article','notif'=>$notif));}
                                                ?>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <!--End Product Info Tabs-->
                            
                        </div>
                        <!--End Post Widget-->

                        <!--Adds Widget-->
                        <?php foreach($this->db->get_where('banner',array('id'=>2))->result() as $b): ?>
                            <div class="sidebar-widget sidebar-adds-widget">
                            	<div class="adds-block" style="background-image:url(<?= base_url('img/banner/'.$b->foto) ?>);">
                                	<div class="inner-box">
                                    	<div class="text"><?= $b->descripcion ?></div>
                                        <a href="<?= $b->enlace ?>" class="theme-btn btn-style-two">información</a>
                                    </div>
                                </div>
                            </div>
                            <!--Ends Adds Widget-->
                        <?php endforeach ?>

                        <!--Widget Column / Instagram Widget-->
	                    <!--Popular Tags Two-->
                        <div class="sidebar-widget popular-tags-two">
                        	<div class="sidebar-title">
                                <h2>Palabras Claves</h2>
                            </div>
                            <?php foreach(explode(',',$this->db->get_where('blog',array('tags !='=>''))->row()->tags) as $t): ?>
                                <a href="<?= base_url('blog/') ?>?direccion=<?= $t ?>"><?= $t ?></a>
                            <?php endforeach ?>
                        </div>
                        
                    </aside>
               	</div>
                
            </div>
            
        </div>
    </div>
    <!--End Sidebar Page Container-->

    <section class="main-slider-six grey-bg">
				    	<div class="auto-container">
				    		<div class="sec-title">
                	<h2>BECAS</h2>
                </div>
				           <div class="tab-column">
				                
				                <!--Default Tab Box-->
				                <div class="default-tab-box tabs-box">
				                    <div class="clearfix">
				                        <?php 
                                            $this->db->limit(6);
                                            $becas = $this->querys->get_blog(array('blog_subcategorias_id'=>11));

                                        ?>
				                        <!--Column-->
				                        <div class="column col-md-8 col-sm-12 col-xs-12">
				                            <!--Tabs Container-->
				                            <div class="tabs-content">
				                            
				                                <!--Tab / Active Tab-->
				                                
                                                <?php foreach($becas->result() as $n=>$b): ?>
                                                    <div class="tab <?= $n==0?'active-tab':'' ?>" id="tab-<?= $b->id ?>">
    				                                    <div class="content">
    				                                        
    				                                        <!--News Block Three-->
    				                                        <div class="news-block-three style-two">
    				                                            <div class="inner-box">
    				                                                <div class="image">
    				                                                    <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$b->foto_800) ?>" alt="" />
    				                                                    <div class="overlay-box">
    				                                                        <div class="content">
    				                                                            <div class="tag"><a href="<?= $b->link ?>"><?= $b->tags ?></a></div>
    				                                                            <h3><a href="<?= $b->link ?>"><?= $b->titulo ?></a></h3>
    				                                                            <!--<ul class="post-meta">
    				                                                                <li><span class="icon qb-clock"></span><?php $b->fecha ?></li>    				                                                                
    				                                                                <li><span class="icon qb-eye"></span><?php $b->vistas ?></li>
    				                                                            </ul>-->
    				                                                        </div>
    				                                                    </div>
    				                                                </div>
    				                                            </div>
    				                                        </div>
    				                                        
    				                                    </div>
    				                                </div>
				                                <?php endforeach ?>
				                                
				                                
				                            </div>
				                        </div>
				                        
				                        <!--Column-->
				                        <div class="column col-md-4 col-sm-12 col-xs-12">
				                            <!--Tab Btns-->
				                            <ul class="tab-btns tab-buttons clearfix">
				                                
                                                <?php foreach($becas->result() as $b): ?>
                                                    <li data-tab="#tab-<?= $b->id ?>" class="tab-btn active-btn">
    				                                    <!--Widget Post-->
    				                                    <article class="widget-post">
    				                                        <figure class="post-thumb">
                                                                <a href="<?= $b->link ?>">
                                                                    <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$b->foto) ?>" alt="<?= $b->titulo ?>">
                                                                </a>
                                                            </figure>
    				                                        <div class="text">
                                                                <a href="<?= $b->link ?>">
                                                                    <?= $b->titulo ?>
                                                                </a>
                                                            </div>
    				                                        <div class="post-info"><?= $b->fecha ?></div>
    				                                    </article>
    				                                </li>
				                                <?php endforeach ?>

				                            </ul>
				                        </div>
				                        
				                    </div>
				                </div>
				                
				            </div>
				                <!--Info Column-->
				           
				        </div>
				    </section>
    
    <!--Blog Gallery-->
    <section class="blog-gallery grey-bg" style="padding-top:40px;">
    	<div class="auto-container">
    		<div class="sec-title">
                	<h2>DESTACADOS</h2>
                </div>

        	<div class="row clearfix">
            	
                
                <?php 
                    $this->db->limit(5);
                    $this->db->order_by('id','DESC');
                    $ultimas_noticias = $this->querys->get_blog(array('destacado'=>1));
                    $n = 1;
                ?>
                <div class="column pull-left col-md-3 col-sm-6 col-xs-12">
                	<?php for($i=0;$i<2; $i++): ?>
                    <!--News Block Two-->
                    <div class="news-block-two small-block">
                        <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>                                       
                            <div class="inner-box">
                                <div class="image">
                                    <a href="<?= $notif->link ?>">
                                        <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="" />
                                    </a>
                                    <div class="category"><a href="<?= $notif->link ?>"><?= $notif->blog_subcategorias->blog_subcategorias_nombre ?></a></div>
                                </div>
                                <div class="lower-box">
                                    <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                    <ul class="post-meta">
                                        <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>
                                    </ul>
                                </div>
                            </div> 
                        <?php endif ?>
                    </div>
                    <?php endfor ?>
                </div>
                
                <div class="column pull-right col-md-3 col-sm-6 col-xs-12">
                	
                    <?php for($i=0;$i<2; $i++): ?>
                        <!--News Block Two-->
                        <div class="news-block-two small-block">
                            <?php if($n<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row($n); $n++; ?>                                       
                                <div class="inner-box">
                                    <div class="image">
                                        <a href="<?= $notif->link ?>">
                                            <img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="" />
                                        </a>
                                        <div class="category"><a href="<?= $notif->link ?>"><?= $notif->blog_subcategorias->blog_subcategorias_nombre ?></a></div>
                                    </div>
                                    <div class="lower-box">
                                        <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                        <ul class="post-meta">
                                            <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>
                                        </ul>
                                    </div>
                                </div> 
                            <?php endif ?>
                        </div>
                    <?php endfor ?>
                    
                </div>
                
                <?php if($ultimas_noticias->num_rows()>0): ?>
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	<?php if(0<$ultimas_noticias->num_rows()): $notif = $ultimas_noticias->row(0); $n++; ?>
                            <!--News Block Seven-->
                            <div class="news-block-seven style-two">
                                <div class="inner-box">
                                    <div class="image">
                                        <a href="<?= $notif->link ?>"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="<?= base_url('img/blog/'.$notif->foto) ?>" alt="" /></a>
                                        <div class="category"><a href="<?= $notif->link ?>"><?= @$notif->blog_subcategorias->blog_subcategorias_nombre ?></a></div>
                                    </div>
                                    <div class="lower-box">
                                        <h3><a href="<?= $notif->link ?>"><?= $notif->titulo ?></a></h3>
                                        <ul class="post-meta">
                                            <li><span class="icon fa fa-clock-o"></span><?= $notif->fecha ?></li>                                        
                                            <li><span class="icon fa fa-eye"></span><?= $notif->vistas ?></li>
                                        </ul>
                                        <div class="text"><?= $notif->texto ?></div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </section>
    <!--End Blog Gallery-->
    
    <!--Instagram Gallery-->
   <!-- <div class="instagram-gallery">
    	<div class="gallery-outer clearfix">
        	
            <h2>Síguenos en Instagram</h2>
            
        	
            <div class="default-portfolio-item">
                <div class="inner-box">
                    <figure class="image-box"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="[base_url]theme/theme/images/gallery/instagram-1.jpg" alt=""></figure>
                    
                    <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                                <a href="[base_url]theme/theme/images/gallery/instagram-1.jpg" class="lightbox-image option-btn" title="Image Caption Here" data-fancybox-group="example-gallery"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="default-portfolio-item">
                <div class="inner-box">
                    <figure class="image-box"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="[base_url]theme/theme/images/gallery/instagram-2.jpg" alt=""></figure>
                    
                    <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                                <a href="[base_url]theme/theme/images/gallery/instagram-2.jpg" class="lightbox-image option-btn" title="Image Caption Here" data-fancybox-group="example-gallery"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="default-portfolio-item">
                <div class="inner-box">
                    <figure class="image-box"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="[base_url]theme/theme/images/gallery/instagram-3.jpg" alt=""></figure>
                    
                    <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                                <a href="[base_url]theme/theme/images/gallery/instagram-3.jpg" class="lightbox-image option-btn" title="Image Caption Here" data-fancybox-group="example-gallery"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="default-portfolio-item">
                <div class="inner-box">
                    <figure class="image-box"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="[base_url]theme/theme/images/gallery/instagram-4.jpg" alt=""></figure>
                    
                    <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                                <a href="[base_url]theme/theme/images/gallery/instagram-4.jpg" class="lightbox-image option-btn" title="Image Caption Here" data-fancybox-group="example-gallery"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="default-portfolio-item">
                <div class="inner-box">
                    <figure class="image-box"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="[base_url]theme/theme/images/gallery/instagram-5.jpg" alt=""></figure>
                    
                    <div class="overlay-box">
                        <div class="overlay-inner">
                            <div class="content">
                                <a href="[base_url]theme/theme/images/gallery/instagram-5.jpg" class="lightbox-image option-btn" title="Image Caption Here" data-fancybox-group="example-gallery"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>
    </div>-->
    
    [footer]