[menu]
<div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align" style="margin-top:10px; background:url([base_url]theme/theme/images/background-documentos.jpg)">
	<div class="realfactory-header-transparent-substitute"></div>
	<div class="realfactory-page-title-overlay"></div>
	<div class="realfactory-page-title-container realfactory-container">
		<div class="realfactory-page-title-content realfactory-item-pdlr">
			<h1 class="realfactory-page-title">Noticias</h1>
			<ul class="page-title-breadcrumb breadcump">
				<li><a href="#"><span class="fa fa-home"></span>Home</a></li>
				<li>Noticias</li>
			</ul>
			
		</div>
	</div>
</div>

<div class="sidebar-page-container">
	<div class="auto-container">
		<div class="row clearfix">
			<div class="tabs-content">
				
				<!--Tab / Active Tab-->
				<div class="tab active-tab" id="economy-all">
					<div class="content">
						
							
							<?php 
								$x = 0;
								foreach($blog->result() as $n=>$b): 
							?>
								<?php if($x==0): ?>
									<div class="row clearfix">
								<?php endif ?>
								<div class="column col-md-4 col-sm-4 col-xs-12">
									
									<!--News Block Two-->
									<div class="news-block-two with-margin">
										<div class="inner-box">
											<div class="image">
												<a href="[link]">
													<img class="wow fadeIn animated" data-wow-delay="0ms" data-wow-duration="2500ms" src="[foto]" alt="" style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
												</a>
												<div class="category"><a href="#">[tags]</a></div>
											</div>
											<div class="lower-box">
												<h3><a href="[link]">[titulo]</a></h3>
												<ul class="post-meta">
													<li><span class="icon fa fa-clock-o"></span>[fecha]</li>
													<!--<li><span class="icon fa fa-comment-o"></span>3</li>-->
													<li><span class="icon fa fa-eye"></span>0</li>
												</ul>
												<div class="text">[texto]</div>
											</div>
										</div>
									</div>
									
								</div>
							<?php $x++; ?>
							<?php if($x==3 || $n==$blog->num_rows()-1): $x = 0; ?>
							</div>
							<?php endif ?>
							<?php endforeach ?>

						
					</div>
				</div>
				
				
				
				
				
			</div>
		</div>
	</div>
</div>
[footer]