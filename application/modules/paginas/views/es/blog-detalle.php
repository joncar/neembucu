[menu]
<!--Blog Single Slider-->
    <div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align" style="margin-top:10px; background:url([base_url]img/blog/[foto_792]); background-size:cover;">
		<div class="realfactory-header-transparent-substitute"></div>
		<div class="realfactory-page-title-overlay"></div>
		<div class="realfactory-page-title-container realfactory-container">
			<div class="realfactory-page-title-content realfactory-item-pdlr">
				<h1 class="realfactory-page-title">[titulo]</h1>
				<ul class="page-title-breadcrumb breadcump">
					<li><a href="#"><span class="fa fa-home"></span>Home</a></li>
					<li>[blog_subcategorias_nombre]</li>
				</ul>
				
			</div>
		</div>
	</div>
    <!--End Blog Single Slider-->
    
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="content">
                    	<div class="blog-single">
                        	

                        	<div class="inner-box">
                            	<div class="upper-box">
                                    <ul class="breadcrumb-bar">
                                        <li><a href="[base_url]">Inicio</a></li>
                                        <li>[blog_categorias_nombre]</li>
                                        <li>[blog_subcategorias_nombre]</li>
                                    </ul>
                                    <!--<ul class="tag-title">
                                        [tags]
                                    </ul>-->
                                    <div class="img">
                                    	<a href="[base_url]/img/blog/[foto_800]" class="lightbox-image" title="[titulo]" alt="[titulo]">
                                    		<img src="[base_url]/img/blog/[foto_800]" alt="" style="width:100%;">
                                    	</a>
                                    </div>                                    
                                    <ul class="post-meta">
                                        <li><span class="icon qb-clock"></span>[fecha]</li>
                                        <li><span class="icon qb-user2"></span>[user]</li>
                                        <!--<li><span class="icon fa fa-comment-o"></span>3 comments</li>-->
                                        <li><span class="icon qb-eye"></span>[vistas] Vistas</li>
                                    </ul>
                                    <ul class="social-icon-one alternate">
                                        <li class="share">Compartir:</li>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=[url]"><span class="fa fa-facebook"></span></a></li>
                                        <li class="twitter"><a href="https://twitter.com/intent/tweet?url=[url]&text=[titulo]"><span class="fa fa-twitter"></span></a></li>
                                        <li class="instagram"><a href="https://plus.google.com/share?url=[url]"><span class="fa fa-google-plus"></span></a></li>
                                        
                                    </ul>
                                </div>
                                <div class="text">
                                	[texto]
                                </div>
                                <!--post-share-options-->
                                <div class="post-share-options">
                                    <div class="tags clearfix">
                                    	[tags]
                                    </div>
                                </div>
                                
                                <!--New Article-->
                                <ul class="new-article clearfix">
                                	<li><a href="[prev]"><span class="fa fa-angle-left"></span> &ensp; &ensp; &ensp; &ensp; Anterior</a></li>
                                    <li><a href="[next]">Siguiente &ensp; &ensp; &ensp; &ensp; <span class="fa fa-angle-right"></span></a></li>
                                </ul>
                            </div>
                            
							<?php if($relacionados->num_rows()>0): ?>
                            <!--Related Posts-->
                            <div class="related-posts">
                            	<div class="sec-title">
                                	<h2>Relacionados</h2>
                                </div>
                                <div class="related-item-carousel owl-carousel owl-theme">
                                	
                                    <!--News Block Two-->
                                    [foreach:relacionados]
                                    <div class="news-block-two small-block">
                                        <div class="inner-box">
                                            <div class="image">
                                                <a href="[link]"><img src="[foto]" alt="" /></a>
                                                <div class="category">
                                                	[tags]
                                                </div>
                                            </div>
                                            <div class="lower-box">
                                                <h3><a href="[link]">[titulo]</a></h3>
                                                <ul class="post-meta">
                                                    <li><span class="icon fa fa-clock-o"></span>[fecha]</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    [/foreach]
                                    
                                    
                                </div>
                            </div>
                           	<?php endif ?>
                            
                            
                            
                        </div>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                	<aside class="sidebar default-sidebar right-sidebar">
					
                    	<!-- Search -->
                        <div class="sidebar-widget search-box">
                        	<form method="post" action="<?= base_url('blog') ?>">
                                <div class="form-group">
                                    <input type="search" name="direccion" value="" placeholder="Buscar" required>
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
						</div>
                        
                        <!--Social Widget-->
                        <div class="sidebar-widget sidebar-social-widget">
                            <div class="sidebar-title">
                                <h2>Siguenos</h2>
                            </div>
                            <ul class="social-icon-one alternate">
                                <li><a href="https://www.facebook.com/GobernaciondelXIIDepartamentodeNeembucu/"><span class="fa fa-facebook"></span></a></li>
	                            <li class="twitter"><a href="https://twitter.com/gobernacionde12"><span class="fa fa-twitter"></span></a></li>                         
                                <li class="instagram"><a href="<?= base_url('contacto.html') ?>"><span class="fa fa-envelope"></span></a></li>
                                <li class="rss"><a href="tel:+595786232210"><span class="fa fa-phone"></span></a></li>
                            </ul>
						</div>
                    	<!--End Social Widget-->
                        
                    	
                        
                        <!--Category Widget-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Categorias</h2>
                            </div>
                            <ul class="cat-list">
                            	[foreach:categorias]
                            		<li class="clearfix"><a href="[link]">[blog_subcategorias_nombre]</a></li>
                            	[/foreach]
                            </ul>
                        </div>
                        <!--End Category Widget-->
                        
                    </aside>
               	</div>
                
            </div>
            
        </div>
    </div>
    <!--End Sidebar Page Container-->
    [footer]