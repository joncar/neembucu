[menu]

<!--<div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align" style="margin-top:10px; background:url([base_url]theme/theme/images/background-secretaria.jpg)">    -->
<div class="realfactory-page-title-wrap  realfactory-style-large realfactory-center-align" style="margin-top:10px; background:url(<?= $banner ?>); background-size:cover">
    <div class="realfactory-header-transparent-substitute"></div>
    <div class="realfactory-page-title-overlay"></div>
    <div class="realfactory-page-title-container realfactory-container">
        <div class="realfactory-page-title-content realfactory-item-pdlr">
            <h1 class="realfactory-page-title"><?= $detail->titulo ?></h1>           
            <ul class="page-title-breadcrumb breadcump">
                <li><a href="#"><span class="fa fa-home"></span>Home</a></li>                
            </ul>   
            
        </div>
    </div>
</div>
    
	<!--About Section-->
    <section class="about-section">
    	
        <div class="row author-info">
        	<div class="col-sm-6 col-xs-12 imageleft" style="background:url(<?= $detail->foto ?>); background-size:cover; background-repeat: no-repeat;">        		
        	</div>
        	<div class="col-sm-6 col-xs-12 content-column ">
        		<div class="content-inner">                    
                    <div class="text">
                        <?= $detail->texto ?>
                    </div>
                </div>
        	</div>
        </div>
        
    </section>
    <!--End About Section-->
[footer]