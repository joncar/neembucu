    <!-- Main Header -->
    <header class="main-header">
    	<!--Header Top-->
    	<div class="header-top">
            <div class="auto-container">
                <div class="clearfix">
                    <!--Top Left-->
                    <div class="top-left col-md-5 col-sm-12 col-xs-12">
                    	<div class="trend" style="color: white;">Tendencias: </div>
                        <div class="single-item-carousel owl-carousel owl-theme" style="margin-left: 10px;">
                        	<?php 
                                $this->db->limit(5);
                                $this->db->order_by('id','DESC');
                                foreach($this->db->get_where('blog',array('status'=>1))->result() as $b): ?>
                            <div class="slide">
                            	<div class="text"><?= substr($b->titulo,0,50).'...'; ?></div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!--Top Right-->
                    <div class="top-right pull-right col-md-7 col-sm-12 col-xs-12">
                        <ul class="top-nav">
                        	
                        </ul>
                        <ul class="english-nav">
                        	<li class="active"><a href="#">Esp</a></li>
                            <li><a href="#">Gua</a></li>                            
                        </ul>
                        <ul class="social-nav">
                        	<li><a href="https://www.facebook.com/GobernaciondelXIIDepartamentodeNeembucu/" target="_blank"><span class="fa fa-facebook-square"></span></a></li>
                            <li><a href="https://twitter.com/Gobneembucu" target="_blank"><span class="fa fa-twitter"></span></a></li>                            
                            <!--<li><a href="#"><span class="fa fa-linkedin-square"></span></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-outer">
                    	<div class="logo"><a href="<?= site_url() ?>"></a></div>
                    </div>
                    
                    <div class="pull-right upper-right clearfix">
                    	<div class="add-image">
                            <a href="#"><img src="[base_url]theme/theme/images/resource/header-add.jpg" alt="" /></a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower">
        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="[base_url]">Inicio</a></li>
                                <li><a href="#">Institucional</a></li>
                                <li class="mega-menu">
                                    <a href="#">Secretaría</a>
                                    <div class="mega-menu-bar">
                                        
                                        <div class="mega-menu-carousel owl-carousel owl-theme">
                                               
                                            <?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>13))->result() as $b): ?>                         
                                                <!--News Block Nine-->
                                                <div class="news-block-nine">
                                                    <div class="inner-box">
                                                        <div class="image">
                                                            <a href="<?= base_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>">
                                                                <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="" />
                                                            </a>
                                                            <div class="category">
                                                                <?= $b->tags ?>
                                                            </div>
                                                        </div>
                                                        <div class="lower-box">
                                                            <h3><a href="<?= base_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>"><?= $b->titulo ?></a></h3>
                                                            <div class="post-date"><?= strftime('%B %d,%Y',strtotime($b->fecha)) ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>

                                            
                                        </div>
                                        
                                    </div>
                                </li>
                                <li class="dropdown">
                                    <a href="#">Documentos</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $b): ?>
                                            <li><a href="<?= base_url('documentos/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li>  
                                <li class="dropdown">
                                    <a href="#">Ley 5189/2014</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $b): ?>
                                            <li><a href="<?= base_url('ley-5189-2014/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Ley 5581/2016</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>4))->result() as $b): ?>
                                            <li><a href="<?= base_url('ley-5589-2016/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Informes</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>5))->result() as $b): ?>
                                            <li><a href="<?= base_url('informes/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Galeria</a>
                                    <ul>
                                        <li><a href="[base_url]fotos.html">Fotos</a></li>
                                        <li><a href="[base_url]videos.html">Videos</a></li>                                        
                                    </ul>
                                </li>                          
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <div class="outer-box">
                    	
                        
                        
                        <!--Search Box-->
                        <div class="search-box-outer">
                            <div class="dropdown">
                                <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                                <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu1">
                                    <li class="panel-outer">
                                        <div class="form-container">
                                            <form method="get" action="<?= base_url('blog') ?>">
                                                <div class="form-group">
                                                    <input type="search" name="direccion" value="" placeholder="Buscar" required>
                                                    <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    
                    <!-- Hidden Nav Toggler -->
                     <div class="nav-toggler">
                         <button class="hidden-bar-opener"><span class="icon qb-menu1"></span></button>
                     </div>
                    
                </div>
            </div>
        </div>
        <!--End Header Lower-->
    	
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="[base_url]" class="img-responsive" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="[base_url]">Inicio</a></li>
                                <li><a href="contact.html">Institucional</a></li>
                                <li class="dropdown">
                                    <a href="#">Secretarías</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>13))->result() as $b): ?>                         
                                            <li><a href="<?= base_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>"><?= $b->titulo ?></a></li>                                        
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#">Documentos</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $b): ?>
                                            <li><a href="<?= base_url('documentos/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li>                                  
                                <li class="dropdown">
                                    <a href="#">Ley 5189/2014</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $b): ?>
                                            <li><a href="<?= base_url('ley-5189-2014/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Ley 5581/2016</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>4))->result() as $b): ?>
                                            <li><a href="<?= base_url('ley-5589-2016/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Informes</a>
                                    <ul>
                                        <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>5))->result() as $b): ?>
                                            <li><a href="<?= base_url('informes/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                                        <?php endforeach ?>                                        
                                    </ul>
                                </li> 
                                <li class="dropdown">
                                    <a href="#">Galeria</a>
                                    <ul>
                                        <li><a href="[base_url]fotos.html">Fotos</a></li>
                                        <li><a href="[base_url]videos.html">Videos</a></li>                                        
                                    </ul>
                                </li> 
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
        
    </header>
    <!--End Header Style Two -->

    <!-- Hidden Navigation Bar -->
    <section class="hidden-bar left-align">
        
        <div class="hidden-bar-closer">
            <button><span class="qb-close-button"></span></button>
        </div>
        
        <!-- Hidden Bar Wrapper -->
        <div class="hidden-bar-wrapper">
            <div class="logo">
                <a href="[base_url]"></a>
            </div>
            <!-- .Side-menu -->
            <div class="side-menu">
                <!--navigation-->
                <ul class="navigation clearfix">
                    <li class="current"><a href="[base_url]">Inicio</a></li>                    
                    <li class="dropdown">
                        <a href="#">Secretarías</a>
                        <ul>
                            <?php foreach($this->db->get_where('blog',array('blog_subcategorias_id'=>13))->result() as $b): ?>                         
                                <li><a href="<?= base_url('secretarias/'.toUrl($b->id.'-'.$b->titulo)) ?>"><?= $b->titulo ?></a></li>                                        
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">Documentos</a>
                        <ul>
                            <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>2))->result() as $b): ?>
                                <li><a href="<?= base_url('documentos/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                            <?php endforeach ?>                                        
                        </ul>
                    </li>                                  
                    <li class="dropdown">
                        <a href="#">Ley 5189/2014</a>
                        <ul>
                            <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>3))->result() as $b): ?>
                                <li><a href="<?= base_url('ley-5189-2014/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                            <?php endforeach ?>                                        
                        </ul>
                    </li> 
                    <li class="dropdown">
                        <a href="#">Ley 5581/2016</a>
                        <ul>
                            <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>4))->result() as $b): ?>
                                <li><a href="<?= base_url('ley-5589-2016/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                            <?php endforeach ?>                                        
                        </ul>
                    </li> 
                    <li class="dropdown">
                        <a href="#">Informes</a>
                        <ul>
                            <?php foreach($this->db->get_where('blog_subcategorias',array('blog_categorias_id'=>5))->result() as $b): ?>
                                <li><a href="<?= base_url('informes/'.toUrl($b->id.'-'.$b->blog_subcategorias_nombre)) ?>"><?= $b->blog_subcategorias_nombre ?></a></li>
                            <?php endforeach ?>                                        
                        </ul>
                    </li> 
                    <li class="dropdown">
                        <a href="#">Galeria</a>
                        <ul>
                            <li><a href="[base_url]fotos.html">Fotos</a></li>
                            <li><a href="[base_url]videos.html">Videos</a></li>                                        
                        </ul>
                    </li> 
                </ul>
            </div>
            <!-- /.Side-menu -->
            
            <!--Options Box-->
            <div class="options-box">
                <!--Sidebar Search-->
                <div class="sidebar-search">
                    <form method="post" action="<?= base_url('blog') ?>" method="get">
                        <div class="form-group">
                            <input type="search" name="direccion" value="" placeholder="Buscar" required="">
                            <button type="submit" class="theme-btn"><span class="fa fa-search"></span></button>
                        </div>
                    </form>
                </div>
                
            </div>
            
        </div><!-- / Hidden Bar Wrapper -->
        
    </section>
    <!-- End / Hidden Bar -->